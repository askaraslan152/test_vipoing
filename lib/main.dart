import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: RandomCheckBoxes(),
    );
  }
}

class RandomCheckBoxes extends StatefulWidget {
  const RandomCheckBoxes({super.key});

  @override
  RandomCheckBoxesState createState() => RandomCheckBoxesState();
}

List<bool> isCheckedList = [];
List<Color> colors = [Colors.blue, Colors.green, Colors.red];

class RandomCheckBoxesState extends State<RandomCheckBoxes> {
  double animationSpeed = 1.0;
  bool colorsAdded = false;

  Map<Color, bool> selectedColors = {};

  @override
  void initState() {
    super.initState();
  }

  void clearCheckBoxes() {
    setState(() {
      isCheckedList.clear();
      selectedColors.clear();
      colors.clear();
      colorsAdded = false;
    });
  }

  void clearColors() {
    setState(() {
      colors.clear();
      colors.addAll([Colors.blue, Colors.green, Colors.red]);
    });
  }

  void addRandomCheckBoxes() {
    if (!colorsAdded) {
      clearColors();
      colorsAdded = true;
    }
    int numOfNewCheckBoxes = 10;
    List<Color> newColors = List.generate(
        numOfNewCheckBoxes, (index) => colors[Random().nextInt(colors.length)]);

    setState(() {
      for (int i = 0; i < numOfNewCheckBoxes; i++) {
        isCheckedList.add(false);
        Color randomColor = newColors[i];
        colors.add(randomColor);

        if (selectedColors.containsKey(randomColor)) {
          isCheckedList[isCheckedList.length - 1] =
              selectedColors[randomColor]!;
        } else {
          selectedColors[randomColor] = false;
        }
      }
    });
  }

  void updateAnimationSpeed(double speed) {
    setState(() {
      animationSpeed = speed;
      for (int i = 0; i < isCheckedList.length; i++) {
        isCheckedList[i] = isCheckedList[i];
      }
    });
  }

  void fillAllCheckboxesWithColor(Color color) {
    setState(() {
      for (int i = 0; i < isCheckedList.length; i++) {
        isCheckedList[i] = colors[i] == color;
      }

      for (int i = 0; i < colors.length; i++) {
        selectedColors[colors[i]] = isCheckedList[i];
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            Expanded(
              child: GridView.builder(
                padding: const EdgeInsets.all(16),
                shrinkWrap: true,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 7,
                  mainAxisSpacing: 16,
                  crossAxisSpacing: 16,
                ),
                itemCount: colors.length,
                itemBuilder: (context, index) {
                  if (index >= isCheckedList.length) {
                    isCheckedList.add(false);
                  }
                  Color randomColor = colors[index];
                  return RoundedCheckBox(
                    isChecked: isCheckedList[index],
                    color: randomColor,
                    borderColor: colors[index],
                    onChanged: (value) {
                      setState(() {
                        isCheckedList[index] = value;
                        if (isCheckedList[index]) {
                          fillAllCheckboxesWithColor(randomColor);
                        }
                      });
                    },
                    animationSpeed: animationSpeed,
                  );
                },
              ),
            ),
            Slider(
              value: animationSpeed,
              min: 0.1,
              max: 5.0,
              divisions: 20,
              onChanged: updateAnimationSpeed,
              label: 'Скорость анимации: ${animationSpeed.toStringAsFixed(2)}',
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: () {
                    addRandomCheckBoxes();
                  },
                  child: const Text('Добавить чек-боксы'),
                ),
                ElevatedButton(
                  onPressed: () {
                    setState(() {
                      clearCheckBoxes();
                    });
                  },
                  child: const Text('Очистить'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class RoundedCheckBox extends StatefulWidget {
  final bool isChecked;
  final Color color;
  final Function(bool) onChanged;
  final double animationSpeed;
  final Color borderColor;

  const RoundedCheckBox({
    super.key,
    required this.isChecked,
    required this.color,
    required this.onChanged,
    required this.animationSpeed,
    required this.borderColor,
  });

  @override
  RoundedCheckBoxState createState() => RoundedCheckBoxState();
}

class RoundedCheckBoxState extends State<RoundedCheckBox>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Color?> _colorAnimation;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: (500 / widget.animationSpeed).round()),
    );

    _colorAnimation = ColorTween(begin: Colors.transparent, end: widget.color)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Curves.easeInOut));

    if (widget.isChecked) {
      _animationController.value = 1.0;
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  void didUpdateWidget(RoundedCheckBox oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.isChecked != oldWidget.isChecked) {
      if (widget.isChecked) {
        _animationController.forward(from: 0.0);
      } else {
        _animationController.reverse(from: 1.0);
      }
    }

    if (widget.animationSpeed != oldWidget.animationSpeed) {
      _animationController.duration =
          Duration(milliseconds: (500 / widget.animationSpeed).round());
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onChanged(!widget.isChecked);

        if (!widget.isChecked) {
          for (int i = 0; i < colors.length; i++) {
            if (colors[i] != widget.color) {
              isCheckedList[i] = false;
            }
          }
        }
      },
      child: AnimatedBuilder(
        animation: _colorAnimation,
        builder: (context, child) {
          return CustomPaint(
            painter: _RoundedCheckBoxPainter(
              isChecked: widget.isChecked,
              color: _colorAnimation.value!,
              borderColor: widget.borderColor,
            ),
            child: SizedBox(
              width: 20.0,
              height: 20.0,
              child: widget.isChecked
                  ? const Center(
                      child: Icon(
                        Icons.check,
                        size: 16.0,
                        color: Colors.white,
                      ),
                    )
                  : null,
            ),
          );
        },
      ),
    );
  }
}

class _RoundedCheckBoxPainter extends CustomPainter {
  final bool isChecked;
  final Color color;
  final Color borderColor;

  _RoundedCheckBoxPainter({
    required this.isChecked,
    required this.color,
    required this.borderColor,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;

    final borderPaint = Paint()
      ..color = borderColor
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;

    final radius = size.width / 2.0;
    final center = Offset(size.width / 2.0, size.height / 2.0);
    canvas.drawCircle(center, radius, paint);
    canvas.drawCircle(center, radius, borderPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
